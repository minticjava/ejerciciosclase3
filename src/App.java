import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        ejercicio5();
    }

    /**Escribe un programa que lea una cantidad de grados centígrados y la pase 
     * a grados Fahrenheit.
     * La fórmula es: F = 32 + ( 9 * C / 5 ) 
     * */
    public static void ejercicio4(){
        try (Scanner sc = new Scanner(System.in)){
            System.out.println("ingrese la temperatura en grados centigrados");
            double a = sc.nextDouble();
            double f;
            f = 32 + (9 * a / 5);
            System.out.println("la temperatura en fahrenheit es: "+f);
        } catch (Exception e) {
            System.out.println("solo numeros");
        }
    }

    /**Escribe un programa java que lea una variable de tipo entero y asígnale un valor.
     * A continuación muestra un mensaje indicando si la variable es par o impar.
     * Utiliza el operador condicional ( ? : ) para resolverlo.
     * Ej:“14espar”o“15esimpar” 
     * */

     public static void ejercicio5(){
         try (Scanner sc = new Scanner(System.in)){
             System.out.println("Ingrese un numero entero");
             int a = sc.nextInt();
             String respuesta = (a % 2 == 0 ? "par" : "impar");
             System.out.println("El numero "+a+" es: "+respuesta);
         } catch (Exception e) {
            System.out.println("solo numeros enteros");
         }
     }
}
